<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="basic-components" xml:lang="ja">

	<info>
		<link type="guide" xref="index#components"/>
		<desc>NOT、AND、OR などの基本的な論理素子</desc>
		<revision pkgversion="2.6" version="0.1" date="2012-10-08" status="final"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="credit.xml"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
	</info>

	<title>基本的な論理素子</title>

	<section id="not">
		<title>NOT</title>
		<p its:translate="no"><media type="image" src="figures/components/not.png">NOT</media></p>
		<p>入力の否定を出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="2"><p/></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<table frame="all" rules="all">
					<tr>
						<td><p>入力端子</p></td>
						<td><p>入力端子の数。2 または 3</p></td>
					</tr>
					<tr>
						<td colspan="2"><p>伝播遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPHL</p></td>
						<td><p>出力がHレベルからLレベルに変化する時の遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPLH</p></td>
						<td><p>出力がLレベルからHレベルに変化する時の遅延時間</p></td>
					</tr>
				</table>
			</item>
		</terms>
	</section>

	<section id="and">
		<title>AND</title>
		<p its:translate="no"><media type="image" src="figures/components/and.png">AND</media></p>
		<p>入力の論理積を出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td colspan="2"><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="3"><p/></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<table frame="all" rules="all">
					<tr>
						<td><p>入力端子</p></td>
						<td><p>入力端子の数。2 または 3</p></td>
					</tr>
					<tr>
						<td colspan="2"><p>伝播遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPHL</p></td>
						<td><p>出力がHレベルからLレベルに変化する時の遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPLH</p></td>
						<td><p>出力がLレベルからHレベルに変化する時の遅延時間</p></td>
					</tr>
				</table>
			</item>
		</terms>
	</section>

	<section id="or">
		<title>OR</title>
		<p its:translate="no"><media type="image" src="figures/components/or.png">OR</media></p>
		<p>入力の論理和を出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td colspan="2"><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="3"><p/></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<table frame="all" rules="all">
					<tr>
						<td><p>入力端子</p></td>
						<td><p>入力端子の数。2 または 3</p></td>
					</tr>
					<tr>
						<td colspan="2"><p>伝播遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPHL</p></td>
						<td><p>出力がHレベルからLレベルに変化する時の遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPLH</p></td>
						<td><p>出力がLレベルからHレベルに変化する時の遅延時間</p></td>
					</tr>
				</table>
			</item>
		</terms>
	</section>

	<section id="xor">
		<title>XOR</title>
		<p its:translate="no"><media type="image" src="figures/components/xor.png">XOR</media></p>
		<p>入力の排他的論理和を出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td colspan="2"><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="3"><p/></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<table frame="all" rules="all">
					<tr>
						<td><p>入力端子</p></td>
						<td><p>入力端子の数。2 または 3</p></td>
					</tr>
					<tr>
						<td colspan="2"><p>伝播遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPHL</p></td>
						<td><p>出力がHレベルからLレベルに変化する時の遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPLH</p></td>
						<td><p>出力がLレベルからHレベルに変化する時の遅延時間</p></td>
					</tr>
				</table>
			</item>
		</terms>
	</section>

	<section id="nand">
		<title>NAND</title>
		<p its:translate="no"><media type="image" src="figures/components/nand.png">NAND</media></p>
		<p>入力の否定論理積を出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td colspan="2"><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="3"><p/></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<table frame="all" rules="all">
					<tr>
						<td><p>入力端子</p></td>
						<td><p>入力端子の数。2 または 3</p></td>
					</tr>
					<tr>
						<td colspan="2"><p>伝播遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPHL</p></td>
						<td><p>出力がHレベルからLレベルに変化する時の遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPLH</p></td>
						<td><p>出力がLレベルからHレベルに変化する時の遅延時間</p></td>
					</tr>
				</table>
			</item>
		</terms>
	</section>

	<section id="nor">
		<title>NOR</title>
		<p its:translate="no"><media type="image" src="figures/components/nor.png">NOR</media></p>
		<p>入力の否定論理和を出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td colspan="2"><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="3"><p/></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td></tr>
					<tr><td><p its:translate="no">L</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td><td><p its:translate="no">L</p></td></tr>
					<tr><td><p its:translate="no">H</p></td><td><p its:translate="no">H</p></td><td><p its:translate="no">L</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<table frame="all" rules="all">
					<tr>
						<td><p>入力端子</p></td>
						<td><p>入力端子の数。2 または 3</p></td>
					</tr>
					<tr>
						<td colspan="2"><p>伝播遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPHL</p></td>
						<td><p>出力がHレベルからLレベルに変化する時の遅延時間</p></td>
					</tr>
					<tr>
						<td><p>tPLH</p></td>
						<td><p>出力がLレベルからHレベルに変化する時の遅延時間</p></td>
					</tr>
				</table>
			</item>
		</terms>
	</section>

	<section id="vdd">
		<title>Vdd</title>
		<p its:translate="no"><media type="image" src="figures/components/vdd.png">Vdd</media></p>
		<p>常時Hレベルを出力します</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="2"><p/></td></tr>
					<tr><td><p its:translate="no">―</p></td><td><p its:translate="no">H</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<p>プロパティーはありません。</p>
			</item>
		</terms>
	</section>

	<section id="gnd">
		<title>GND</title>
		<p its:translate="no"><media type="image" src="figures/components/gnd.png">GND</media></p>
		<p>常時Lレベルを出力します。</p>
		<terms>
			<item>
				<title>真理値表:</title>
				<table frame="all" rules="all" shade="colgroups">
					<colgroup><col/></colgroup>
					<colgroup><col/></colgroup>
					<tr><td><p>入力</p></td><td><p>出力</p></td></tr>
					<tr><td colspan="2"><p/></td></tr>
					<tr><td><p its:translate="no">―</p></td><td><p its:translate="no">L</p></td></tr>
				</table>
			</item>
			<item>
				<title>プロパティー:</title>
				<p>プロパティーはありません。</p>
			</item>
		</terms>
	</section>

</page>
