# -*- coding: utf-8; indent-tabs-mode: t; tab-width: 4 -*-

import math
from glogic import config, const
from glogic.Components import comp_dict
from gettext import gettext as _
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject

class ComponentWindow(Gtk.Window):

	__gsignals__ = {
		'component-checked': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
		'window-hidden': (GObject.SIGNAL_RUN_FIRST, None, ())
	}

	def __init__(self):

		Gtk.Window.__init__(self, title=_("Components"))
		self.set_type_hint(Gdk.WindowTypeHint.DIALOG)
		self.set_resizable(False)
		self.checked_widget = None

		# Create toggle buttons
		self.button_names = [
			const.component_NOT,     const.component_AND,     const.component_OR,      const.component_XOR,
			const.component_NAND,    const.component_NOR,     const.component_RSFF,    const.component_JKFF,
			const.component_DFF,     const.component_TFF,     const.component_counter, const.component_adder,
			const.component_SISO,    const.component_SIPO,    const.component_PISO,    const.component_PIPO,
			const.component_SW,      const.component_7seg,    const.component_LED,     const.component_VDD,
			const.component_GND,     const.component_OSC,     const.component_probe,   const.component_text
		]

		columns = 4
		rows = math.ceil(len(self.button_names) / columns)
		layout = Gtk.Table(5, 4, True)
		self.add(layout)

		self.buttons = []
		for i, name in enumerate(self.button_names):
			button = Gtk.ToggleButton()
			icon = GdkPixbuf.Pixbuf.new_from_file(config.DATADIR+"/images/components/"+name+".png")
			button.add(Gtk.Image.new_from_pixbuf(icon))
			button.set_tooltip_text(comp_dict[name].description)
			button.connect("clicked", self.on_button_clicked)
			self.buttons.append(button)
			layout.attach(button, i % columns, i % columns + 1, i / columns, i / columns + 1)

		self.connect("delete-event", self.on_window_delete)

	def on_button_clicked(self, button):
		if button.get_active() and button is not self.checked_widget:
			self.check_button(button)
		elif button is self.checked_widget:
			self.check_button(None)

	def uncheck_all_buttons(self):
		for btn in self.buttons:
			btn.set_active(False)

	def check_button(self, button):
		old_checked = self.checked_widget
		self.checked_widget = button
		checked_component = const.component_none
		if button is not None:
			for (i, btn) in enumerate(self.buttons):
				if btn is old_checked:
					btn.set_active(False)
				if btn is button:
					if not btn.get_active():
						btn.set_active(True)
					checked_component = self.button_names[i]

		self.emit("component-checked", checked_component)

	def on_window_delete(self, widget, event):
		self.uncheck_all_buttons()
		self.emit("component-checked", const.component_none)
		self.emit("window-hidden")
		return True
