# -*- coding: utf-8; indent-tabs-mode: t; tab-width: 4 -*-

from glogic import const
from gi.repository import Gtk, Gdk, GObject
from gettext import gettext as _

class PropertyWindow(Gtk.Window):

	__gsignals__ = {
		'property-changed': (GObject.SIGNAL_RUN_FIRST, None, ()),
		'window-hidden': (GObject.SIGNAL_RUN_FIRST, None, ())
	}

	def __init__(self):

		self.title = _("Properties")
		Gtk.Window.__init__(self, title=self.title)
		self.set_resizable(False)
		self.set_type_hint(Gdk.WindowTypeHint.DIALOG)
		self.set_border_width(5)

		# Buttons
		buttons = Gtk.Box(spacing=5)
		self.undo_btn = Gtk.Button(stock=Gtk.STOCK_UNDO)
		buttons.pack_start(self.undo_btn, False, False, 0)
		self.apply_btn = Gtk.Button(stock=Gtk.STOCK_APPLY)
		buttons.pack_start(self.apply_btn, False, False, 0)

		self.bottom_btns = Gtk.Alignment.new(1, 0, 0, 0)
		self.bottom_btns.add(buttons)

		self.vbox = None

		self.prop_controls = []

		self.apply_btn.connect("clicked", self.on_apply_btn_clicked)
		self.undo_btn.connect("clicked", self.on_undo_btn_clicked)
		self.connect("delete-event", self.on_window_delete)

	def on_property_changed(self, widget):
		self.apply_btn.set_sensitive(True)
		self.undo_btn.set_sensitive(True)

	def on_apply_btn_clicked(self, widget):
		values = []
		i = 0
		for p in self.component.properties:
			if isinstance(p[1], tuple):
				if p[1][0] == const.property_select:
					values.append(self.prop_controls[i].get_active())
				elif p[1][0] == const.property_int:
					self.prop_controls[i].update()
					values.append(int(self.prop_controls[i].get_value()))
				elif p[1][0] == const.property_float:
					self.prop_controls[i].update()
					values.append(self.prop_controls[i].get_value())
				else:
					values.append(self.prop_controls[i].get_text())
			elif p[1] == const.property_bool:
				values.append(self.prop_controls[i].get_active())
			else:
				i -= 1
			i += 1

		if self.component.propertyChanged(values):
			dialog = Gtk.MessageDialog(self, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK, _("Set values are invalid."))
			dialog.run()
			dialog.destroy()
		else:
			self.component.values = values
			self.component.set_rot_props()
			self.apply_btn.set_sensitive(False)
			self.undo_btn.set_sensitive(False)
			self.emit("property-changed")

	def on_undo_btn_clicked(self, widget):
		i = 0
		for p in self.component.properties:
			if isinstance(p[1], tuple):
				if p[1][0] == const.property_select:
					self.prop_controls[i].set_active(self.component.values[i])
				elif p[1][0] == const.property_int:
					self.prop_controls[i].set_value(self.component.values[i])
				elif p[1][0] == const.property_float:
					self.prop_controls[i].set_value(self.component.values[i])
				else:
					self.prop_controls[i].set_text(self.component.values[i])
			elif p[1] == const.property_bool:
				self.prop_controls[i].set_active(self.component.values[i])
			else:
				i -= 1
			i += 1

		self.apply_btn.set_sensitive(False)
		self.undo_btn.set_sensitive(False)

	def setComponent(self, component):

		self.component = component
		if self.vbox is not None:
			self.vbox.remove(self.bottom_btns)
			self.remove(self.vbox)
		self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
		if component is None:
			self.vbox.pack_start(Gtk.Label(_("Please select a component to edit properties.")), True, True, 5)
			self.set_title(self.title)

		else:
			self.set_title("%s - %s" % (self.title, component.description))
			if len(component.properties) != 0:
				layout = Gtk.Table(len(component.properties), 2, False)
				i = 0
				# Create property editor
				self.prop_controls = []
				for (j, p) in enumerate(component.properties):
					caption = Gtk.Label(p[0])
					caption.set_alignment(1.0, 0.5)
					layout.attach(caption, 0, 1, j, j + 1)
					has_property = True
					if isinstance(p[1], tuple):
						if p[1][0] == const.property_select:
							ctrl = Gtk.ComboBoxText()
							choices = p[1][1:]
							for choice in choices:
								ctrl.append_text(choice)
							ctrl.set_active(self.component.values[i])
							ctrl.connect("changed", self.on_property_changed)
						elif p[1][0] == const.property_int:
							ctrl = Gtk.SpinButton()
							ctrl.set_increments(1, 10)
							ctrl.set_range(p[1][1], p[1][2])
							ctrl.set_value(component.values[i])
							ctrl.connect("changed", self.on_property_changed)
							ctrl.connect("activate", self.on_apply_btn_clicked)
							ctrl.set_size_request(p[1][3], -1)
						elif p[1][0] == const.property_float:
							ctrl = Gtk.SpinButton()
							ctrl.set_increments(1, 10)
							ctrl.set_range(p[1][1], p[1][2])
							ctrl.set_digits(p[1][3])
							ctrl.set_value(component.values[i])
							ctrl.connect("changed", self.on_property_changed)
							ctrl.connect("activate", self.on_apply_btn_clicked)
							ctrl.set_size_request(p[1][4], -1)
						else:
							ctrl = Gtk.Entry()
							ctrl.set_text(component.values[i])
							ctrl.connect("changed", self.on_property_changed)
							ctrl.connect("activate", self.on_apply_btn_clicked)
							ctrl.set_width_chars(p[1][1])
					elif p[1] == const.property_bool:
						ctrl = Gtk.CheckButton("")
						ctrl.set_active(component.values[i])
						ctrl.connect("toggled", self.on_property_changed)
						ctrl.connect("activate", self.on_apply_btn_clicked)
					else:
						ctrl = Gtk.Label("")
						i -= 1
						has_property = False
					i += 1

					propbox = Gtk.Box()
					propbox.pack_start(ctrl, False, False, 3)
					if has_property:
						self.prop_controls.append(ctrl)
					propbox.pack_start(Gtk.Label(p[2]), False, False, 3)
					layout.attach(propbox, 1, 2, j, j + 1)

				self.vbox.pack_start(layout, True, True, 10)

			else:
				self.vbox.pack_start(Gtk.Label(_("This component has no property.")), True, True, 5)

		self.vbox.pack_start(self.bottom_btns, False, False, 5)
		self.apply_btn.set_sensitive(False)
		self.undo_btn.set_sensitive(False)
		self.add(self.vbox)
		self.vbox.show_all()

	def on_window_delete(self, widget, event):
		self.emit("window-hidden")
		return True
